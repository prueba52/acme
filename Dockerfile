FROM openjdk:8-jdk-buster
MAINTAINER bryan.manzabal@ug.edu.ec
COPY ./AcmeAPI/target/*.jar app.jar
RUN rm /etc/localtime && \
ln -s /usr/share/zoneinfo/America/Guayaquil /etc/localtime && \
mkdir -p app/build/config && \
chmod 777 app
EXPOSE 1001
ENTRYPOINT ["java","-XX:+UseG1GC","-Dfile.encoding=UTF-8","-Duser.timezone=America/guayaquil","-jar","/app.jar","--spring.config.location=file:/app/build/config/application.properties"]
