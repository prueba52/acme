package com.ec.acme.app.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ec.acme.app.model.RequestLogin;
import com.ec.acme.app.model.RequestUser;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseLogin;
import com.ec.acme.app.model.ResponseUser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

public interface UserApi {

	@HystrixCommand(fallbackMethod = "reliableResponse", commandKey = "create", groupKey = "UserApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@PostMapping("/create")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> create(@RequestBody RequestUser request);
	
	
	@HystrixCommand(fallbackMethod = "reliableResponse", commandKey = "update", groupKey = "UserApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@PostMapping("/update")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> update(@RequestBody RequestUser request);
	
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> reliableResponse(@RequestBody RequestUser request);
	
	@HystrixCommand(fallbackMethod = "reliableDelete", commandKey = "delete", groupKey = "UserApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@DeleteMapping("/delete/{userId}")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> delete(@PathVariable String userId);
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> reliableDelete(@PathVariable String userId);
	
	@HystrixCommand(fallbackMethod = "reliableFindAll", commandKey = "findAll", groupKey = "UserApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@GetMapping("/find")
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseUser> findAll();
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseUser> reliableFindAll();
	
	@HystrixCommand(fallbackMethod = "reliableFindAll", commandKey = "find", groupKey = "UserApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@GetMapping("/find/{name}")
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseUser> find(@PathVariable String email);
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseUser> reliableFind(@PathVariable String email);
	
	@HystrixCommand(fallbackMethod = "reliableLogin", commandKey = "login", groupKey = "UserApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@PostMapping("/login")
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseLogin> login(@RequestBody RequestLogin request);
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseLogin> reliableLogin(@RequestBody RequestLogin request);
}
