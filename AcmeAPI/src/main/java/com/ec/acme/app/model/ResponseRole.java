package com.ec.acme.app.model;

import java.util.List;

public class ResponseRole extends Response{

	private static final long serialVersionUID = 1L;
	private List<RequestRole> roles;
	public List<RequestRole> getRoles() {
		return roles;
	}
	public void setRoles(List<RequestRole> roles) {
		this.roles = roles;
	}
	@Override
	public String toString() {
		return "ResponseRole [roles=" + roles + "]";
	}
}
