package com.ec.acme.app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ec.acme.app.model.RequestApp;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseApp;
import com.ec.acme.app.service.AppService;

@RestController
@RequestMapping("/app")
public class AppApiController implements AppApi{
	
	@Autowired
	private AppService appService;

	@Override
	public ResponseEntity<Response> create(RequestApp request) {
		return new ResponseEntity<>(appService.save(request), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> update(RequestApp request) {
		return new ResponseEntity<>(appService.update(request), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> reliableResponse(RequestApp request) {
		return new ResponseEntity<>(appService.responseGeneric(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> delete(String appId) {
		return new ResponseEntity<>(appService.delete(appId), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> reliableDelete(String appId) {
		return new ResponseEntity<>(appService.responseGeneric(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseApp> findAll() {
		return new ResponseEntity<>(appService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseApp> reliableFindAll() {
		ResponseApp response = (ResponseApp) appService.responseGeneric();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseApp> find(String name) {
		return new ResponseEntity<>(appService.find(name), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseApp> reliableFind(String name) {
		ResponseApp response = (ResponseApp) appService.responseGeneric();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
