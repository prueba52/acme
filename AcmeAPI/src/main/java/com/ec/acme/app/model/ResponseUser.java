package com.ec.acme.app.model;

import java.util.List;

public class ResponseUser extends Response{

	private static final long serialVersionUID = 1L;
	private List<RequestUser> users;
	public List<RequestUser> getUsers() {
		return users;
	}
	public void setUsers(List<RequestUser> users) {
		this.users = users;
	}
	@Override
	public String toString() {
		return "ResponseUser [users=" + users + "]";
	}
}
