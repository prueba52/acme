package com.ec.acme.app.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.ec.acme.app.document.App;

public class RequestUser extends RequestLogin{

	private static final long serialVersionUID = 1L;
	@Id
	private String userId;
	private String firdtName;
	private String lastName;
	private String image;
	@DBRef
	private List<App> app;
	@Indexed(unique = false, direction = IndexDirection.DESCENDING)
	private String status;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirdtName() {
		return firdtName;
	}
	public void setFirdtName(String firdtName) {
		this.firdtName = firdtName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<App> getApp() {
		return app;
	}
	public void setApp(List<App> app) {
		this.app = app;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "RequestUser [userId=" + userId + ", firdtName=" + firdtName + ", lastName=" + lastName + ", image="
				+ image + ", app=" + app + ", status=" + status + "]";
	}
}
