package com.ec.acme.app.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import com.ec.acme.app.document.Role;

public class RequestApp implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	private String appId;
	@Indexed(unique = true, direction = IndexDirection.DESCENDING)
	private String name;
	@DBRef
	private List<Role> role;
	@Indexed(unique = false, direction = IndexDirection.DESCENDING)
	private String status;
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Role> getRole() {
		return role;
	}
	public void setRole(List<Role> role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "RequestApp [appId=" + appId + ", name=" + name + ", role=" + role + ", status=" + status + "]";
	}
}
