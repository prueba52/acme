package com.ec.acme.app.model;

import java.io.Serializable;

import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

public class RequestLogin implements Serializable{

	private static final long serialVersionUID = 1L;
	private String password;
	@Indexed(unique = true, direction = IndexDirection.DESCENDING)
	private String email;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "RequestLogin [password=" + password + ", email=" + email + "]";
	}
}
