package com.ec.acme.app.model;

import java.util.List;

public class ResponseApp extends Response{

	private static final long serialVersionUID = 1L;
	private List<RequestApp> apps;
	public List<RequestApp> getApps() {
		return apps;
	}
	public void setApps(List<RequestApp> apps) {
		this.apps = apps;
	}
	@Override
	public String toString() {
		return "ResponseApp [apps=" + apps + "]";
	}
}
