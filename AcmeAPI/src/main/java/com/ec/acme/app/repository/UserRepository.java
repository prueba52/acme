package com.ec.acme.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ec.acme.app.document.User;

@Repository
public interface UserRepository extends MongoRepository<User, String>{

	public List<User> findByEmail(String email);
	public List<User> findByEmailAndStatus(String email, String status);
}
