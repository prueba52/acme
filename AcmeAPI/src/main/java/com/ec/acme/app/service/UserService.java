package com.ec.acme.app.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ec.acme.app.document.User;
import com.ec.acme.app.model.RequestLogin;
import com.ec.acme.app.model.RequestUser;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseLogin;
import com.ec.acme.app.model.ResponseUser;
import com.ec.acme.app.repository.UserRepository;
import com.ec.acme.app.util.Encryptation;

@Service
@Transactional
public class UserService {

	private static final String MESSAGEERROR = "Lo sentimos, intentelo nuevo.";
	private static final String MESSAGEFIND = "Se hallaron exitosamente los usuarios.";
	
	@Value("${encrytion.key}")
	private String key;
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private Encryptation encryp;
	
	public Response save(RequestUser request) {
		Response response = new Response();
		try {
			User user = new User();
			user.setFirdtName(request.getFirdtName());
			user.setLastName(request.getLastName());
			user.setEmail(request.getEmail());
			user.setImage(request.getImage());
			user.setApp(request.getApp());
			user.setPassword(encryp.encode(request.getPassword(), key));
			user.setStatus(request.getStatus());
			user.setCreateDate(new Date());
			user.setModifyDate(new Date());
			userRepository.save(user);
			response.setCode("0");
			response.setMessage("Se guardo exitosamente el usuario.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response update(RequestUser request) {
		Response response = new Response();
		try {
			User user = userRepository.findByEmail(request.getEmail()).stream().findFirst().orElse(null);
			if(user != null) {
				user.setFirdtName((request.getFirdtName() == null)?user.getFirdtName():request.getFirdtName());
				user.setLastName((request.getLastName() == null)?user.getLastName():request.getLastName());
				user.setEmail((request.getEmail() == null)?user.getEmail():request.getEmail());
				user.setPassword(encryp.encode(request.getPassword(), key));
				user.setImage((request.getImage() == null)?user.getImage():request.getImage());
				user.setApp(request.getApp());
				user.setStatus((request.getStatus() == null)?user.getStatus():request.getStatus());
				user.setModifyDate(new Date());
				userRepository.save(user);
				response.setCode("0");
				response.setMessage("Se actualizo exitosamente el usuario.");
			}else {
				response.setCode("1");
				response.setMessage("No existe el usuario a actualizar.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response delete(String userId) {
		Response response = new Response();
		try {
			userRepository.deleteById(userId);
			response.setCode("0");
			response.setMessage("Se elimino exitosamente el usuario.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public ResponseUser findAll() {
		ResponseUser response = new ResponseUser();
		try {
			response.setUsers(new ArrayList<>());
			userRepository.findAll().forEach(u -> response.getUsers().add(u));
			response.setCode("0");
			response.setMessage(MESSAGEFIND);
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public ResponseUser find(String email) {
		ResponseUser response = new ResponseUser();
		try {
			response.setUsers(new ArrayList<>());
			userRepository.findByEmail(email).forEach(u -> response.getUsers().add(u));
			response.setCode("0");
			response.setMessage(MESSAGEFIND);
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public ResponseLogin login(RequestLogin request) {
		ResponseLogin response = new ResponseLogin();
		try {
			User user = userRepository.findByEmail(request.getEmail()).stream().findFirst().orElse(null);
			if(user == null){
				response.setCode("1");
				response.setMessage(".");
			}else if(!user.getPassword().equals(encryp.encode(request.getPassword(), key))) {
				response.setCode("1");
				response.setMessage("Clave invalida.");
			}else if(!user.getStatus().equals("A")){
				response.setCode("2");
				response.setMessage("Usuario inactivo.");
			}else {
				response.setCode("0");
				response.setMessage(MESSAGEFIND);
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response responseGeneric() {
		Response response = new Response();
		response.setCode("-2");
		response.setMessage("Lo sentimos, su transacción esta tardando un poco mas de lo esperado, intentelo nuevamente.");
		return response;
	}
}
