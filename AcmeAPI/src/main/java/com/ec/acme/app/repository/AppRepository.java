package com.ec.acme.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ec.acme.app.document.App;

@Repository
public interface AppRepository extends MongoRepository<App, String>{

	public List<App> findByName(String name);
	public List<App> findByNameAndStatus(String name, String status);
}
