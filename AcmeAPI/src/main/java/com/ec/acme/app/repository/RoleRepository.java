package com.ec.acme.app.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ec.acme.app.document.Role;

@Repository
public interface RoleRepository extends MongoRepository<Role, String>{

	public List<Role> findByName(String name);
	public List<Role> findByNameAndStatus(String name, String status);
}
