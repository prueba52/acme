package com.ec.acme.app.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ec.acme.app.model.RequestRole;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseRole;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

public interface RoleApi {

	@HystrixCommand(fallbackMethod = "reliableResponse", commandKey = "create", groupKey = "RoleApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@PostMapping("/create")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> create(@RequestBody RequestRole request);
	
	
	@HystrixCommand(fallbackMethod = "reliableResponse", commandKey = "update", groupKey = "RoleApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@PostMapping("/update")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> update(@RequestBody RequestRole request);
	
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> reliableResponse(@RequestBody RequestRole request);
	
	@HystrixCommand(fallbackMethod = "reliableDelete", commandKey = "delete", groupKey = "RoleApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@DeleteMapping("/delete/{roleId}")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> delete(@PathVariable String roleId);
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> reliableDelete(@PathVariable String roleId);
	
	@HystrixCommand(fallbackMethod = "reliableFindAll", commandKey = "findAll", groupKey = "RoleApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@GetMapping("/find")
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseRole> findAll();
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseRole> reliableFindAll();
	
	@HystrixCommand(fallbackMethod = "reliableFindAll", commandKey = "find", groupKey = "RoleApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@GetMapping("/find/{name}")
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseRole> find(@PathVariable String name);
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseRole> reliableFind(@PathVariable String name);
}
