package com.ec.acme.app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ec.acme.app.model.RequestRole;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseRole;
import com.ec.acme.app.service.RoleService;

@RestController
@RequestMapping("/role")
public class RoleApiController implements RoleApi{
	
	@Autowired
	private RoleService roleService;
	
	@Override
	public ResponseEntity<Response> create(RequestRole request) {
		return new ResponseEntity<>(roleService.save(request), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> update(RequestRole request) {
		return new ResponseEntity<>(roleService.update(request), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> reliableResponse(RequestRole request) {
		return new ResponseEntity<>(roleService.responseGeneric(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> delete(String roleId) {
		return new ResponseEntity<>(roleService.delete(roleId), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> reliableDelete(String roleId) {
		return new ResponseEntity<>(roleService.responseGeneric(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseRole> findAll() {
		return new ResponseEntity<>(roleService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseRole> reliableFindAll() {
		ResponseRole response = (ResponseRole) roleService.responseGeneric();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseRole> find(String name) {
		return new ResponseEntity<>(roleService.find(name), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseRole> reliableFind(String name) {
		ResponseRole response = (ResponseRole) roleService.responseGeneric();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
