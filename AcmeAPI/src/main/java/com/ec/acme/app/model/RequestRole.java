package com.ec.acme.app.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

public class RequestRole implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	private String roleId;
	@Indexed(unique = true, direction = IndexDirection.DESCENDING)
	private String name;
	private String description;
	@Indexed(unique = false, direction = IndexDirection.DESCENDING)
	private String status;
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "RequestRole [roleId=" + roleId + ", name=" + name + ", description=" + description + ", status="
				+ status + "]";
	}
}
