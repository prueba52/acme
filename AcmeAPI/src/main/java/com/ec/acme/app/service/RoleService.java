package com.ec.acme.app.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ec.acme.app.document.Role;
import com.ec.acme.app.model.RequestRole;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseRole;
import com.ec.acme.app.repository.RoleRepository;

@Service
@Transactional
public class RoleService {

	private static final String MESSAGEERROR = "Lo sentimos, intentelo nuevo.";
	
	@Autowired
	private RoleRepository roleRepository;
	
	public Response save(RequestRole request) {
		Response response = new Response();
		try {
			Role role = new Role();
			role.setName(request.getName());
			role.setDescription(request.getDescription());
			role.setStatus(request.getStatus());
			role.setCreateDate(new Date());
			role.setModifyDate(new Date());
			roleRepository.save(role);
			response.setCode("0");
			response.setMessage("Se guardo exitosamente el role.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response update(RequestRole request) {
		Response response = new Response();
		try {
			Role role = roleRepository.findByName(request.getName()).stream().findFirst().orElse(null);
			if(role != null) {
				role.setName((request.getName() == null)?role.getName():request.getName());
				role.setDescription((request.getDescription() == null)?role.getDescription():request.getDescription());
				role.setStatus((request.getStatus() == null)?role.getStatus():request.getStatus());
				role.setModifyDate(new Date());
				roleRepository.save(role);
				response.setCode("0");
				response.setMessage("Se actualizo exitosamente el role.");
			}else {
				response.setCode("1");
				response.setMessage("No existe el role.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response delete(String roleId) {
		Response response = new Response();
		try {
			roleRepository.deleteById(roleId);
			response.setCode("0");
			response.setMessage("Se elimino exitosamente el role.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public ResponseRole findAll() {
		ResponseRole response = new ResponseRole();
		try {
			response.setRoles(new ArrayList<>());
			roleRepository.findAll().forEach(r -> response.getRoles().add(r));
			response.setCode("0");
			response.setMessage("Se hallaron exitosamente los roles.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public ResponseRole find(String name) {
		ResponseRole response = new ResponseRole();
		try {
			response.setRoles(new ArrayList<>());
			roleRepository.findByName(name).forEach(r -> response.getRoles().add(r));
			response.setCode("0");
			response.setMessage("Se hallaron exitosamente los roles.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response responseGeneric() {
		Response response = new Response();
		response.setCode("-2");
		response.setMessage("Lo sentimos, su transacción esta tardando un poco mas de lo esperado, intentelo nuevamente.");
		return response;
	}
}
