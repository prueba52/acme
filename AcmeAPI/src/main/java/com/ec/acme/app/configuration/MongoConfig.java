package com.ec.acme.app.configuration;

import org.springframework.context.annotation.Bean; 
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver; 
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver; 
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper; 
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

@Configuration 
public class MongoConfig {

	@Bean 
	public MappingMongoConverter mappingMongoConverter(MongoDatabaseFactory mongoDbFactory, MongoMappingContext mongoMappingContext) {
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
		MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver,mongoMappingContext); 
		converter.setTypeMapper(new DefaultMongoTypeMapper(null));
		return converter; 
	} 

	@Bean
	public GridFsTemplate gridFsTemplate(MongoDatabaseFactory mongoDbFactory, MongoConverter mongoConverter) throws Exception {
		return new GridFsTemplate(mongoDbFactory, mongoConverter);
	}
}
