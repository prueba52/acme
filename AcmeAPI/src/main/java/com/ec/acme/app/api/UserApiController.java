package com.ec.acme.app.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ec.acme.app.model.RequestLogin;
import com.ec.acme.app.model.RequestUser;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseLogin;
import com.ec.acme.app.model.ResponseUser;
import com.ec.acme.app.service.UserService;

@RestController
@RequestMapping("/user")
public class UserApiController implements UserApi{

	@Autowired
	private UserService userService;
	
	@Override
	public ResponseEntity<Response> create(RequestUser request) {
		return new ResponseEntity<>(userService.save(request), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> update(RequestUser request) {
		return new ResponseEntity<>(userService.update(request), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> reliableResponse(RequestUser request) {
		return new ResponseEntity<>(userService.responseGeneric(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> delete(String userId) {
		return new ResponseEntity<>(userService.delete(userId), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Response> reliableDelete(String userId) {
		return new ResponseEntity<>(userService.responseGeneric(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseUser> findAll() {
		return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseUser> reliableFindAll() {
		ResponseUser response = (ResponseUser) userService.responseGeneric();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseUser> find(String email) {
		return new ResponseEntity<>(userService.find(email), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseUser> reliableFind(String name) {
		ResponseUser response = (ResponseUser) userService.responseGeneric();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseLogin> login(RequestLogin request) {
		return new ResponseEntity<>(userService.login(request), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResponseLogin> reliableLogin(RequestLogin request) {
		ResponseLogin response = (ResponseLogin) userService.responseGeneric();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
