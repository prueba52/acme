package com.ec.acme.app.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ec.acme.app.model.RequestApp;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseApp;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

public interface AppApi {

	@HystrixCommand(fallbackMethod = "reliableResponse", commandKey = "create", groupKey = "AppApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@PostMapping("/create")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> create(@RequestBody RequestApp request);
	
	
	@HystrixCommand(fallbackMethod = "reliableResponse", commandKey = "update", groupKey = "AppApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@PostMapping("/update")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> update(@RequestBody RequestApp request);
	
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> reliableResponse(@RequestBody RequestApp request);
	
	@HystrixCommand(fallbackMethod = "reliableDelete", commandKey = "delete", groupKey = "AppApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@DeleteMapping("/delete/{appId}")
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> delete(@PathVariable String appId);
	@CrossOrigin(origins= "*")
	public ResponseEntity<Response> reliableDelete(@PathVariable String appId);
	
	@HystrixCommand(fallbackMethod = "reliableFindAll", commandKey = "findAll", groupKey = "AppApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@GetMapping("/find")
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseApp> findAll();
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseApp> reliableFindAll();
	
	@HystrixCommand(fallbackMethod = "reliableFindAll", commandKey = "find", groupKey = "AppApi", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000"),
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "5") })
	@GetMapping("/find/{name}")
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseApp> find(@PathVariable String name);
	@CrossOrigin(origins= "*")
	public ResponseEntity<ResponseApp> reliableFind(@PathVariable String name);
}
