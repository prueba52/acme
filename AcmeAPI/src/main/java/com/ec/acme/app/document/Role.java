package com.ec.acme.app.document;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import com.ec.acme.app.model.RequestRole;

@Document(collection = "ROLE")
public class Role extends RequestRole{

	private static final long serialVersionUID = 1L;
	private Date createDate;
	private Date modifyDate;
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	@Override
	public String toString() {
		return "Role [createDate=" + createDate + ", modifyDate=" + modifyDate + "]";
	}
}
