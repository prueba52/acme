package com.ec.acme.app;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.ec.acme.app.document.Role;
import com.ec.acme.app.document.User;
import com.ec.acme.app.repository.RoleRepository;
import com.ec.acme.app.repository.UserRepository;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.ec.acme.app")
@EnableCircuitBreaker
@EnableHystrix
@EnableAutoConfiguration
@EnableSwagger2
@EnableMongoRepositories(basePackages = "com.ec.acme.app.repository")
public class AppApiApplication implements CommandLineRunner{

	@Value("${user.master}")
	private String userMaster;
	@Value("${pass.master}")
	private String passMaster;
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(AppApiApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try {			
			List<User> users = userRepository.findByEmail(userMaster);
			if(users.isEmpty()) {
				User user = new User();
				user.setEmail(userMaster);
				user.setFirdtName(userMaster);
				user.setLastName(userMaster);
				user.setPassword(passMaster);
				user.setStatus("A");
				user.setCreateDate(new Date());
				user.setModifyDate(new Date());
				userRepository.save(user);
				System.out.println("Se registro el usuario maestro.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
