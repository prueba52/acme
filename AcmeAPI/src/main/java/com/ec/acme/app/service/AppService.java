package com.ec.acme.app.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ec.acme.app.document.App;
import com.ec.acme.app.model.RequestApp;
import com.ec.acme.app.model.Response;
import com.ec.acme.app.model.ResponseApp;
import com.ec.acme.app.repository.AppRepository;

@Service
@Transactional
public class AppService {
	
	private static final String MESSAGEERROR = "Lo sentimos, intentelo nuevo.";

	@Autowired
	private AppRepository appRepository;
	
	public Response save(RequestApp request) {
		Response response = new Response();
		try {
			App app = new App();
			app.setName(request.getName());
			app.setStatus(request.getStatus());
			app.setCreateDate(new Date());
			app.setModifyDate(new Date());
			appRepository.save(app);
			response.setCode("0");
			response.setMessage("Se guardo exitosamente la aplicacion.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response update(RequestApp request) {
		Response response = new Response();
		try {
			App app = appRepository.findByName(request.getName()).stream().findFirst().orElse(null);
			if(app != null) {
				app.setName((request.getName() == null)?app.getName():request.getName());
				app.setStatus((request.getName() == null)?app.getName():request.getName());
				app.setModifyDate(new Date());
				appRepository.save(app);
				response.setCode("0");
				response.setMessage("Se actualizo exitosamente la aplicacion.");
			}else {
				response.setCode("0");
				response.setMessage("No existe la aplicacion.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response delete(String appId) {
		Response response = new Response();
		try {
			appRepository.deleteById(appId);
			response.setCode("0");
			response.setMessage("Se elimino exitosamente la aplicacion.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public ResponseApp findAll() {
		ResponseApp response = new ResponseApp();
		try {
			response.setApps(new ArrayList<>());
			appRepository.findAll().forEach(a -> response.getApps().add(a));
			response.setCode("0");
			response.setMessage("Se hallaron exitosamente las aplicaciones.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public ResponseApp find(String name) {
		ResponseApp response = new ResponseApp();
		try {
			response.setApps(new ArrayList<>());
			appRepository.findByName(name).forEach(a -> response.getApps().add(a));
			response.setCode("0");
			response.setMessage("Se hallaron exitosamente las aplicaciones.");
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode("-1");
			response.setMessage(MESSAGEERROR);
		}
		return response;
	}
	
	public Response responseGeneric() {
		Response response = new Response();
		response.setCode("-2");
		response.setMessage("Lo sentimos, su transacción esta tardando un poco mas de lo esperado, intentelo nuevamente.");
		return response;
	}
}