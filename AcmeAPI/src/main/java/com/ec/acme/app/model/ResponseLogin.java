package com.ec.acme.app.model;

import java.util.List;

public class ResponseLogin extends Response{

	private static final long serialVersionUID = 1L;
	private List<RequestRole> role;
	public List<RequestRole> getRole() {
		return role;
	}
	public void setRole(List<RequestRole> role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "ResponseLogin [role=" + role + "]";
	}
}
